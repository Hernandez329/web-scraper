from django.urls import path
from cars.views import show_cars


urlpatterns = [
    path('list/', show_cars, name="cars_list")
]